"""
The classStructure module is used by the mpy_module_generator to prepare the structures which are written then to
the module file
"""


class classStructure(object):
    name = ""
    variables = []
    methods = []

    def __init__(self, name):
        print("creating class: " + name)
        self.name = name
        self.variables = []
        self.methods = []

    def addVariable(self, type, name):
        self.variables.append(classStructure.variable(type, name))
        return len(self.variables)

    def addMethod(self, name):
        self.methods.append(classStructure.method(name))
        return len(self.methods)

    class variable():
        type = ""
        name = ""

        def __init__(self, type, name):
            print("creating variable: " + type + " " + name)
            self.name = name
            self.type = type

    class method():
        name = ""
        arguments = []

        def __init__(self, name):
            print("creating method: " + name)
            self.name = name
            self.arguments = []

        def addArgument(self, type, name):
            print("adding argument: " + type + " " + name + " to method: " + self.name)
            self.arguments.append(classStructure.variable(type, name))
            return len(self.arguments)


class virtualFunction(object):
    name = ""
    arguments = []

    def __init__(self, name):
        print("creating virtual function: " + name)
        self.name = name
        self.arguments = []

    def addArgument(self, type, name):
        print("adding argument: " + type + " " + name + " to virtual function: " + self.name)
        self.arguments.append(virtualFunction.variable(type, name))
        return len(self.arguments)

    class variable():
        type = ""
        name = ""

        def __init__(self, type, name):
            print("creating variable in virtual function: " + type + " " + name)
            self.name = name
            self.type = type
