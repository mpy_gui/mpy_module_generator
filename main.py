"""
The mpy_module_generator builds templates for built-in MicroPython modules. Built-in modules are necessary to access
hardware driver code which is written in C from the running Python script.

In the MicroPython module generator there can be defined virtual functions (functions which are accessed without a class),
 classes, and class methods

Usage:
- set the variable mname e.g.: mname = "test" - (to import in Python script: import test)
- set filename of the module, beginning with "mod", e.g.: filename = "modtest.c"

- To define virtual functions:
    virtualfunctions.append(virtualfunction("functionname")

    -> then a list is initialized which begins at 0 for the first function.

    - define the argument(s) with type mp_obj_t e.g.:
        virtfunctions[0].addArgument("mp_obj_t", "argumentname")

- To define classes:
    classes.append(classStructure("classname"))

    -> then a list is initialized which begins at 0 for the first class.

    - define class variable(s) with any type e.g.:
        classes[0].addVariable("int", "varname")

    - define class method(s) e.g.:
        classes[0].addMethod("methodname")

    -> then a second list is initialized inside of the class which begins at 0 for the first method

        - define the argument(s) with type mp_obj_t for the method e.g.:
            classes[0].methods[0].addArgument("mp_obj_t", "val")


When everything is defined, the script can be run and will create the C-file which has to be included into the project.
To compile it successfully, consider the comments at the end of the file and paste the corresponding block of code into
the files qstrdefsport.h and mpconfigport.h.

Further, search the file for "TODO". At these points the C code of the drivers and the conversion of the mp_obj_t type
into a C type has to be implemented.
"""



from definitions import *
from classStructure import *
########################################################################################################################
### set values below:

# module name
mname = "test"
filename = "modtest.c"
outfile = open(filename, "w")

# Create virtual functions
virtfunctions =[]

# 0 pyexec_friendly_repl
virtfunctions.append(virtualFunction("testfunc1"))
virtfunctions[0].addArgument("mp_obj_t", "arg1")
virtfunctions[0].addArgument("mp_obj_t", "arg2")

virtfunctions.append(virtualFunction("testfunc2"))
virtfunctions[1].addArgument("mp_obj_t", "arg1")

'''
# 1 get_color
virtfunctions.append(virtualFunction("get_color"))

# 2 set_thickness
virtfunctions.append(virtualFunction("set_thickness"))
virtfunctions[2].addArgument("mp_obj_t", "value")

# 3 get_thickness
virtfunctions.append(virtualFunction("get_thickness"))

# 4 draw_circle
virtfunctions.append(virtualFunction("draw_circle"))
virtfunctions[4].addArgument("mp_obj_t", "x")
virtfunctions[4].addArgument("mp_obj_t", "y")
virtfunctions[4].addArgument("mp_obj_t", "radius")

# 5 draw_line
virtfunctions.append(virtualFunction("draw_line"))
virtfunctions[5].addArgument("mp_obj_t", "x1")
virtfunctions[5].addArgument("mp_obj_t", "y1")
virtfunctions[5].addArgument("mp_obj_t", "x2")
virtfunctions[5].addArgument("mp_obj_t", "y2")

# 6 draw_pixel
virtfunctions.append(virtualFunction("draw_pixel"))
virtfunctions[6].addArgument("mp_obj_t", "x")
virtfunctions[6].addArgument("mp_obj_t", "y")

# 7 draw_rectangle
virtfunctions.append(virtualFunction("draw_rectangle"))
virtfunctions[7].addArgument("mp_obj_t", "x")
virtfunctions[7].addArgument("mp_obj_t", "y")
virtfunctions[7].addArgument("mp_obj_t", "width")
virtfunctions[7].addArgument("mp_obj_t", "height")
'''

# Create classes
classes = []
classes.append(classStructure("Test"))
classes[0].addVariable("int", "val_a")
classes[0].addVariable("int", "val_b")
classes[0].addMethod("change_val_a")
classes[0].methods[0].addArgument("mp_obj_t", "val")

#classes[0].addVariable("int", "param2")
#classes[0].addVariable("mp_obj_t", "callback")

#classes[0].methods[0].addArgument("mp_obj_t", "callbackfunc")

# classes.append(classStructure("timer"))
# classes[1].addVariable("uint8_t", "hello_number")
# classes[1].addMethod("increment")
# classes[1].addMethod("setval")


########################################################################################################################
# functions for writing to file
def comment (str, ident = 0):
    for i in range(ident):
        outfile.write("    ")
    outfile.write("// "+str+"\n")
def newline(num=1):
    for i in range(num):
        outfile.write("\n")
def write(str, ident = 0):
    for i in range(ident):
        outfile.write("    ")
    outfile.write(str)

# Begin to write into file...
write(fileinfo)
write(includes)
newline(2)
if (len(classes) > 0):
    comment("CLASSES")
    for c in classes:
        comment("class "+ c.name)
        write("const mp_obj_type_t "+mname+"_"+c.name+"Obj_type;\n")
        write("typedef struct _"+mname+"_"+c.name+"_obj_t {\n")
        comment("base represents some basic information, like type", 1);
        write("mp_obj_base_t base;\n",1)
        comment("a member created by us",1)
        for v in c.variables:
            write(v.type + " "+ v.name+";\n",1)
        write("} "+mname+"_"+c.name+"_obj_t;\n")
        newline(1)
        write("mp_obj_t "+mname+"_"+c.name+"_make_new(const mp_obj_type_t *type, size_t n_args, size_t n_kw, const mp_obj_t *args) {\n")
        comment("this checks the number of arguments",1)
        comment("on error -> raise python exception", 1)
        write("mp_arg_check_num(n_args, n_kw, "+str(len(c.variables))+", "+str(len(c.variables))+", true);\n",1)
        comment("create a new object of our C-struct type", 1)
        write(mname+"_"+c.name+"_obj_t *self = m_new_obj("+mname+"_"+c.name+"_obj_t);\n",1)
        comment("give it a type",1)
        write("self->base.type = &"+mname+"_"+c.name+"Obj_type;\n",1)
        for v in range(len(c.variables)):
            #TODO: adapt Function "mp_obj_get_int" to argument type
            write("self->"+ c.variables[v].name+" = mp_obj_get_int(args["+str(v)+"]);\n",1)
        write("return MP_OBJ_FROM_PTR(self);\n}\n",1)
        newline()
        write("STATIC void "+mname+"_"+c.name+"_print(const mp_print_t *print, mp_obj_t self_in, mp_print_kind_t kind) {\n")
        write(mname+"_"+c.name+"_obj_t *self = MP_OBJ_TO_PTR(self_in);\n",1)
        for v in range(len(c.variables)):
            #TODO: adapt Function print to argument type
            write("printf(\"\\n\\rclass: "+c.name+", variable: " + c.variables[v].name +", type: "+ c.variables[v].type+", value: %u\", self->"+c.variables[v].name+");\n",1)
        write("}\n")
        newline()
        for m in range (len(c.methods)):
            comment("class "+ c.name+ ", method "+ c.methods[m].name)
            write("STATIC mp_obj_t "+mname+"_"+c.name+"_"+c.methods[m].name+"(mp_obj_t self_in")
            if (len(c.methods[m].arguments) > 0):
                write(", ")
            for a in range (len(c.methods[m].arguments)):
                write(c.methods[m].arguments[a].type+" "+c.methods[m].arguments[a].name)
                if (a < len(c.methods[m].arguments) -1):
                    write(", ")
            write(") {\n")
            write(mname+"_"+c.name+"_obj_t *self = MP_OBJ_TO_PTR(self_in);\n",1)
            comment("TODO: Insert code of class method",1)
            newline(2)
            write("return mp_const_none;\n}\n",1)
            write("MP_DEFINE_CONST_FUN_OBJ_"+str(len(c.methods[m].arguments)+1)+"("+mname+"_"+c.name+"_"+c.methods[m].name+"_obj, "+mname+"_"+c.name+"_"+c.methods[m].name+");\n")
        newline()
        comment("map class "+ c.name+ " methods")
        write("STATIC const mp_rom_map_elem_t "+ mname+"_"+c.name+"_locals_dict_table[] = {\n")
        for m in range (len(c.methods)):
            write("{ MP_ROM_QSTR(MP_QSTR_"+c.methods[m].name+"), MP_ROM_PTR(&"+mname+"_"+c.name+"_"+c.methods[m].name+"_obj) },\n",1)
        write("};\n")
        write("STATIC MP_DEFINE_CONST_DICT("+mname+"_"+c.name+"_locals_dict, "+mname+"_"+c.name+"_locals_dict_table);\n")
        newline()
        write("const mp_obj_type_t "+mname+"_"+c.name+"Obj_type = {\n")
        write("{ &mp_type_type },\n",1)
        write(".name = MP_QSTR_"+c.name+"Obj,\n",1)
        write(".print = "+mname+"_"+c.name+"_print,\n",1)
        write(".make_new = "+mname+"_"+c.name+"_make_new,\n",1)
        write(".locals_dict = (mp_obj_dict_t*)&"+mname+"_"+c.name+"_locals_dict,\n",1)
        write("};\n")
        comment("end of class "+c.name)
        newline(2)
    comment("END OF CLASSES")
    newline(2)
if (len(virtfunctions) > 0):
    comment("VIRTUAL FUNCTIONS")
    for vrt in range(len(virtfunctions)):
        comment("virtual function "+virtfunctions[vrt].name)
        write("STATIC mp_obj_t "+mname+"_"+virtfunctions[vrt].name+"(")
        for var in range (len(virtfunctions[vrt].arguments)):
            write(virtfunctions[vrt].arguments[var].type+" "+virtfunctions[vrt].arguments[var].name)
            if (var < len(virtfunctions[vrt].arguments)-1):
                write(", ")
        write(") {\n")
        comment("TODO: Insert function code",1)
        newline(2)
        write("return mp_const_none;\n}\n", 1)
        write("STATIC MP_DEFINE_CONST_FUN_OBJ_"+str(len(virtfunctions[vrt].arguments))+"("+mname+"_"+virtfunctions[vrt].name+"_obj, "+mname+"_"+virtfunctions[vrt].name+");\n\n")
    comment("END OF VIRTUAL FUNCTIONS\n")

comment("MODULE GLOBAL TABLE (all classes, all static methods)")
write("STATIC const mp_map_elem_t "+mname+"_globals_table[] = {\n")
write("{ MP_OBJ_NEW_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_"+mname+") },\n",1)
for vrt in range(len(virtfunctions)):
    write("{ MP_OBJ_NEW_QSTR(MP_QSTR_"+virtfunctions[vrt].name+"), (mp_obj_t)&"+mname+"_"+virtfunctions[vrt].name+"_obj},\n",1)
for c in classes:
    write("{ MP_OBJ_NEW_QSTR(MP_QSTR_"+c.name+"Obj), (mp_obj_t)&"+mname+"_"+c.name+"Obj_type },\n",1)
write("};\n")
write("STATIC MP_DEFINE_CONST_DICT(mp_module_"+mname+"_globals, "+mname+"_globals_table);\n")
newline(2)
comment("ADD MODULE TO MODULE GLOBALS (do not edit)")
write("const mp_obj_module_t mp_module_"+mname+" = {\n")
write(".base = { &mp_type_module },\n",1)
write(".globals = (mp_obj_dict_t*)&mp_module_"+mname+"_globals,\n",1)
write("};\n")
newline(3)
write("/*\n// TODO: Paste following code into qstrdefsport.h\n")
newline()
write("Q("+mname+")\n")
for c in classes:
    write("Q("+c.name+"Obj)\n")
for c in classes:
    for m in range (len(c.methods)):
        write("Q("+c.methods[m].name+")\n")
for vrt in range(len(virtfunctions)):
    write("Q("+virtfunctions[vrt].name+")\n")
newline()
write("*/")
newline(2)
write("/*\n// TODO: Paste following code into mpconfigport.h\n")
newline()
write("// included Modules:\n\n")
write("extern const struct _mp_obj_module_t mp_module_"+mname+";\n\n",1)
write("// #define MICROPY_PORT_BUILTIN_MODULES\n\n")
write("{ MP_OBJ_NEW_QSTR(MP_QSTR_"+mname+"), (mp_obj_t)&mp_module_"+mname+" },\n",1)
newline()
write("*/")
