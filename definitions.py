fileinfo = "/*\n File Created by Module and Class Generator for Micropython\n Author: Adam Lang \n More information about Micropython modules: \n https://forum.micropython.org/viewtopic.php?t=3274\n http://micropython-dev-docs.readthedocs.io/en/latest/adding-module.html\n*/\n\n"

includes = "#include \"py/nlr.h\"\n#include \"py/obj.h\"\n#include \"py/runtime.h\"\n#include \"py/binary.h\"\n#include \"bsp.h\"\n#include \"system_definitions.h\""
