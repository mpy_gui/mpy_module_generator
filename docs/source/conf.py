# -*- coding: utf-8 -*-
# Configuration file for the Sphinx documentation builder.
import sys
# reload(sys)
# sys.setdefaultencoding('utf8')


import os
import sys
sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('../'))
sys.path.insert(0, os.path.abspath('../../'))
project = 'mpy_module_generator'
copyright = '2018, Novotronik Software'
extensions = ['autoapi.extension',]
autoapi_type = 'python'
proj_folder = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..'))
autoapi_dirs = []
try:
	if os.listdir(os.path.abspath(os.path.join(proj_folder, '.'))):
		autoapi_dirs.append(os.path.abspath(os.path.join(proj_folder, '.')))
except OSError:
	pass

print("PYTHON SOURCE FILES FOR AUTOAPI GENERATION (autoapi_dirs value in conf.py): ", autoapi_dirs)
#autoapi_dirs.append(os.path.abspath(os.path.join(proj_folder,'subfolder')))
source_suffix = '.rst'
master_doc = 'index'
html_theme = 'nature'

''' This function returns the nesting of the folder a in relation to the proj_folder
'''
def get_dir_depth(a):
    a= "".join(a.rsplit(proj_folder))
    return a.count('/')

''' This function builds a tree structure beginning from *rootDir* and filters out the directories which are contained in the autoapi_dirs setting
    It filters out the .py files and writes the index.html file. In the browser, the folders can be accessed and there is a link to the RAW file such as to the HTML representation of the source code file.
    It uses JavaScript and the MetroUI TreeView (https://metroui.org.ua/treeview.html)
'''
def write_tree_to_index_html (rootDir):
    dirs =[]
    tmplist = []
    with open ("/home/rtd/rtd/checkouts/readthedocs.org/user_builds/"+str(context['slug'])+"/artifacts/"+str(context['current_version'])+"/sphinx/rtdcodeview/index.html", 'w+') as index:
        index.write('<!DOCTYPE html>\n<html lang="en">\n<head>\n<!-- Required meta tags -->\n<meta charset="utf-8"\n<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">\n<!-- Metro 4 -->\n<link rel="stylesheet" href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css">\n  </head>')
        index.write("<body>\n<h1>"+str(context['slug'])+" - Source Browser</h1>\n")
        index.write("<br>\n")
        index.write('<ul data-role="treeview">\n')
        for dirName, subdirList, fileList in os.walk(rootDir):
            dirs.append(dirName)
        #Filter directories
        for el in dirs:
            #Filter pattern
            if '/docs' in el or "/.git" in el or el not in autoapi_dirs:
                #print("Filter",el)
                tmplist.append(el)
        for el in tmplist:
            dirs.remove(el)
        for el in dirs:
            for i in range (get_dir_depth(el)):
                index.write('<ul>\n')
            index.write('\t<li data-icon="<span class=\'mif-folder-open\'></span>" data-caption="'+os.path.basename(el)+'">\n')
            pathstr = el[el.find(str(context['current_version']))+ len(str(context['current_version'])) + 1:]
            if len(pathstr) > 0:
                pathstr = pathstr +'/'
            index.write('<ul>\n')
            for f in os.listdir(el):
                #filter files
                if f.endswith(".py"):
                    index.write('\t<li data-icon="<span class=\'mif-file-text\'></span>" data-caption="'+f+'">&thinsp;<a href="'+pathstr+f+'">[RAW]</a>&thinsp;<a href="'+pathstr+str(f)[:-3]+'.html\">[HTML]</a></li>\n')
            index.write('</ul>\n')
            for i in range (get_dir_depth(el)):
                index.write('</ul>\n')
        index.write('<!-- jQuery first, then Metro UI JS -->\n<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>\n<script src="https://cdn.metroui.org.ua/v4/js/metro.min.js"></script>\n</body>\n</html>')
        index.write('<br>')
        index.write('<form action="../index.html">    <input type="submit" value="Back" /> </form>')

def gen_html_pages():
    for mypath in autoapi_dirs:
        onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
        for file in onlyfiles:
            if file.endswith(".py"):
                pathstr = mypath[mypath.find(str(context['current_version']))+ len(str(context['current_version'])) + 1:]
                if len(pathstr) > 0:
                    pathstr = pathstr +'/'
                else:
                    pathstr = './'
                filename_short = file[:-3]
                with open (os.path.join("/home/rtd/rtd/checkouts/readthedocs.org/user_builds/"+str(context['slug'])+"/artifacts/"+str(context['current_version'])+"/sphinx/rtdcodeview/", pathstr + filename_short+".html"), "w+") as out:
                    import pygments
                    from pygments import highlight
                    from pygments.lexers import PythonLexer
                    from pygments.formatters import HtmlFormatter
                    with open(os.path.join("/home/rtd/rtd/checkouts/readthedocs.org/user_builds/"+str(context['slug'])+"/artifacts/"+str(context['current_version'])+"/sphinx/rtdcodeview/", pathstr + file), "r") as code:
                        code = code.read()
                        out.write(highlight(code, PythonLexer(), pygments.formatters.get_formatter_by_name('html',linenos='table', noclasses=True, style='colorful')))

def setup_rtd_source_browser(app,exception):
    try:
        if exception == None:
            cmd = "ln -sf /home/rtd/rtd/checkouts/readthedocs.org/user_builds/"+str(context['slug'])+"/checkouts/"+str(context['current_version'])+" /home/rtd/rtd/checkouts/readthedocs.org/user_builds/"+str(context['slug'])+"/artifacts/"+str(context['current_version'])+"/sphinx/rtdcodeview"
            os.system(cmd)
            write_tree_to_index_html(proj_folder)
            gen_html_pages()
    except IOError:
        pass

def setup(app):
    app.connect('build-finished', setup_rtd_source_browser)

'''
#Unknown modules are searched and mocked out in order to make source code browsing possible
import ast
import mock

libnames =[]
for dir in autoapi_dirs:
    files = os.listdir( dir )
    for el in files:
        if el.endswith(".py"):
            file = (os.path.abspath(os.path.join(dir, el)))
            with open (file) as f:
                import_string = f.readlines()
            import_string = ("".join(import_string))
            for node in ast.iter_child_nodes(ast.parse(import_string)):
                if isinstance(node, ast.ImportFrom):
                    if not node.names[0].asname:  # excluding the 'as' part of import
                        libnames.append(node.module)
                elif isinstance(node, ast.Import): # excluding the 'as' part of import
                    if not node.names[0].asname:
                        libnames.append(node.names[0].name)

for libname in libnames:
    try:
        lib = __import__(libname)
    except:
        sys.modules[libname] = mock.MagicMock()
'''