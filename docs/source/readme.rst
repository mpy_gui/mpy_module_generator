Description
==============

The mpy_module_generator allows to create templates for builtin modules. The modules can be included into the MPLABX Project to access hardware ressources and the project has to be recompiled.

Steps:

1. define the module by modifying the definitions.py file
2. Run the script, there will be generated a .c file
3. Include the .c file in the projects, consider the comments at the end of the .c file
4. Fill the .c file which is a template with the hardware access code (e.g. call to a driver)
5. Recompile the project and program the hardware
6. Access the module with the use of your Python Script which is running on the platform
5. Access the 
